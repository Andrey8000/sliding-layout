package crocodile8000.slidingsample;

import crocodile8000.sliding.*;

import java.util.Random;

import android.os.Bundle;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.Toast;

public class ActivityStart extends FragmentActivity {

	SlidingLayout sl;
	Random rnd;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.lay_start2);

		sl = (SlidingLayout) findViewById(R.id.slidingLay1);
		rnd = new Random();
		
		sl.setIgnoredSlidingView((HorizontalScrollView) findViewById(R.id.scrollViewHorizontal));
	}

	
	
	
	public void clickBt1(View v){
		sl.setBackgroundColor(Color.rgb(rnd.nextInt(250), rnd.nextInt(250), rnd.nextInt(250)));
	}

	
	
	
	public void clickBt2(View v){
		Toast.makeText(getApplicationContext(), "It's Sliding panel", Toast.LENGTH_SHORT).show();
	}
	
	
	
	
	public void about(View v){
		Toast.makeText(getApplicationContext(), "My e-mail: crocodile8000@gmail.com \nor: Andrey-r.com", Toast.LENGTH_LONG).show();
	}

}
