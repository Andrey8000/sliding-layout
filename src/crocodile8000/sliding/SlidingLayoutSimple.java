package crocodile8000.sliding;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;



/**
 * !Deprecated!
 * SlidingLaySimple - ��� ������� �������� �������
 * 
 * ���������� ������ layout, �������� ���������� � ������� TranslateAnimation, 
 * ��� ��������� onTouchEvent - ����������� ������� ������� .layout(...),
 * ��������� (��� ���) ������� �� �������� - � ������ onInterceptTouchEvent(...)
 * ������� ������ ������� - int maxX
 * @author Crocodile8008
 *
 */

public class SlidingLayoutSimple extends LinearLayout {
	
	private int toleranceToMove, maxX, dividerX;
	
	private int tX, tXstart, tXinner, tYstart, xOld;
	private int layW, layH, layXcurr;
	private boolean isLeftSlide, canMove = false;
	Animation movement;

	
	
	@SuppressLint("NewApi")
	public SlidingLayoutSimple(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public SlidingLayoutSimple(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SlidingLayoutSimple(Context context) {
		super(context);
	}

	
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout( changed,  l,  t,  r,  b);
		if (layW <1) layW = getWidth();
		if (layH <1) layH  = getHeight();
		toleranceToMove = layW/10;
		maxX = layW - layW/3;
		dividerX = layW/3;
		layXcurr = this.getLeft();
	}

	

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		
		if (event.getAction() != MotionEvent.ACTION_MOVE) canMove = true;
		isLeftSlide = false;
		
	    if (event.getAction() == MotionEvent.ACTION_DOWN) {
			canMove = false;
	    	tYstart = (int)event.getY();
	    	tXstart = (int)event.getX();
	    } 
	    else if (event.getAction() == MotionEvent.ACTION_MOVE) {
	        if (
	        Math.abs(tYstart - event.getY()) > toleranceToMove 
	        || Math.abs(tXstart - event.getX()) > toleranceToMove) {
		        canMove = true;
		        if (Math.abs(tYstart - event.getY()) >  Math.abs(tXstart - event.getX())){
		        	isLeftSlide = false; }
		        else{
		        	isLeftSlide = true; 
					tXinner = (int)event.getX();
					xOld = (int)event.getX()-tXinner;	
		        }
	        }
	    } 
	    else if (event.getAction() == MotionEvent.ACTION_UP) isLeftSlide = false;

	    return isLeftSlide;
	}

	
	
    @Override
    public boolean onTouchEvent(MotionEvent event) {
		
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			canMove= true;
			tXinner = (int)event.getX();
			xOld = (int)event.getX()-tXinner;
		}
		
		tX = (int)event.getX()-tXinner;
		
		if(canMove){
			checkXLimits(false);
			movement = new TranslateAnimation(xOld , tX , 0 , 0);
			movement.setDuration(0);
			movement.setFillAfter(true);
		    this.startAnimation(movement);
		    xOld = tX;
		}
		
		if (event.getAction() == MotionEvent.ACTION_UP) {
			checkXLimits(true);
	        this.layout((int)tX, 0, (int)(tX+layW), (int)layH);
	        this.clearAnimation();
	        try{
	        	View parent = (View) this.getParent();
	        	if (parent != null){
	        		parent.postInvalidate();
	        	}
	        }
	        catch(Exception e){ }
	        movement = null;
		}
		
		return true;
    }
    
    
    
    /**
     * �������� �� �������� �� X �� ���������� ������� + �������
     */
    private void checkXLimits(boolean endPositionCheck){
    	
    	// ����������� � �� ����� ��������
		if (tX >= maxX) tX = maxX;
		if (layXcurr < dividerX && tX < 0) tX = 0;
		if (layXcurr > dividerX && tX > 0) tX = 0; 
		if (layXcurr > dividerX && tX < -maxX) tX = -maxX;
		
		// ������� ����� ����������
		if (endPositionCheck) {
			// ���� ��� ����� ��������� �� ������ ��������
			if (layXcurr < dividerX){
				if (tX < dividerX/2) tX=0; // ������� ������� �� dividerX/2
				else tX = maxX;
			}
			// ������
			else{
				if (tX < -dividerX/4) tX=0;
				else tX = maxX;
			}
		}
    }
    
    
}

